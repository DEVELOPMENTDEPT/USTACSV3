<header class="app-header bg-dark border-0 navbar">
    <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto ml-3" type="button" data-toggle="sidebar-show"><span class="navbar-toggler-icon"></span></button><a class="navbar-brand" href="#"><b>UST</b><span>ACS </span><small>V3</small></a>
    <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show"><span class="navbar-toggler-icon"></span></button>
    <ul class="nav navbar-nav d-md-down-none">
    <li class="nav-item px-3"><a class="nav-link" href="#">Dashboard</a></li>
    <li class="nav-item px-3"><a class="nav-link" href="#">Users</a></li>
    <li class="nav-item px-3"><a class="nav-link" href="#">Settings</a></li>
    </ul>
    <ul class="nav navbar-nav ml-auto">
    <li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="icon-bell"></i><span class="badge badge-pill badge-danger">5</span></a></li>
    <li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="icon-list"></i></a></li>
    <li class="nav-item d-md-down-none"><a class="nav-link" href="#"><i class="icon-location-pin"></i></a></li>
    <li class="nav-item dropdown"><a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="lalse"><img class="img-avatar" src="img/avatars/6.jpg" alt="admin@bootstrapmaster.com"></a>
        <div class="dropdown-menu dropdown-menu-right">
        <div class="dropdown-header"><strong>Account</strong></div><a class="dropdown-item" href="#"><i class="la la-bill"></i> Updates<span class="badge badge-info">42</span></a><a class="dropdown-item" href="#"><i class="la la-envelope-o"></i> Messages<span class="badge badge-success">42</span></a><a class="dropdown-item" href="#"><i class="la la-tasks"></i> Tasks<span class="badge badge-danger">42</span></a><a class="dropdown-item" href="#"><i class="la la-comments"></i> Comments<span class="badge badge-warning">42</span></a>
        <div class="dropdown-header"><strong>Settings</strong></div><a class="dropdown-item" href="#"><i class="la la-user"></i> Profile</a><a class="dropdown-item" href="#"><i class="la la-wrench"></i> Settings</a><a class="dropdown-item" href="#"><i class="la la-usd"></i> Payments<span class="badge badge-secondary">42</span></a><a class="dropdown-item" href="#"><i class="la la-file"></i> Projects<span class="badge badge-primary">42</span></a>
        <div class="dropdown-divider"></div><a class="dropdown-item" href="#"><i class="la la-shield"></i> Lock Account</a>
        <a class="dropdown-item" href="{{ route('logout') }}"
        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();" aria-labelledby="navbarDropdown">

            <i class="la la-lock"></i> {{ __('Logout') }}</a>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
        </div>
    </li>
    </ul>
    <!--button class="navbar-toggler aside-menu-toggler d-md-down-none" type="button" data-toggle="aside-menu-lg-show"><span class="navbar-toggler-icon"></span></button>
    <button class="navbar-toggler aside-menu-toggler d-lg-none mr-3" type="button" data-toggle="aside-menu-show"><span class="navbar-toggler-icon"></span></button-->
</header>
