<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  @include('layouts.shared.head')
  <body class="app flex-row align-items-center">
    @yield('content')
    <!-- CoreUI and necessary plugins-->
    @include('layouts.shared.scripts')
  </body>
</html>
