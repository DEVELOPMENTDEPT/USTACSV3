@extends('layouts.master')

@section('content')
<div class="card">
    <div class="card-header"><strong>Roles</strong><small>Form</small></div>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <form action=" {{ route('roles.store') }}">
                        <label for="name">Name</label>
                        <input class="form-control" id="name" type="text" placeholder="Enter role name">
                        @role('super-admin', 'admin')
                            <input class="btn btn-primary" id ="send" type="submit" value="{{ __('save') }}">
                        @else
                            {{ __('I am not a super-admin...') }}
                        @endrole

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
