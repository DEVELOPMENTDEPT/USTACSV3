@extends('layouts.master')

@section('content')
   <div class="container">
        <div class="card shadow mb-2">
            <div class="card-header py-2">
              <h3 class="m-0 font-weight-bold text-primary">Users</h3>
              <a class="btn  btn-primary active pull-right" type="button" href="{{route('users.create') }}" aria-pressed="true">Create</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row"><div class="col-sm-12 col-md-6">
                <div class="dataTables_length" id="dataTable_length">
                </div></div><div class="col-sm-12 col-md-6"><div id="dataTable_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="dataTable"></label></div></div></div><div class="row"><div class="col-sm-12"><table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                  <thead>
                    <tr role="row">
                    <th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 161px;">{{__('User Name')}}</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 246px;">{{__('Email')}}</th>
                    <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 115px;">{{__('Actions')}}</th>
                  </thead>
                  <tbody>
                    @forelse($result as $user)
                        <tr role="row" class="odd">
                            <td class="sorting_1">{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{__('Actions')}}</td>
                        </tr>
                    @empty
                        <p> No Data To Show</p>
                    @endforelse

                    </tr></tbody>
                </table>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-5">
                    <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">
                        Showing 1 to 10 of {{$result->count()}} entries
                    </div>
                </div>
                <div class="col-sm-12 col-md-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">
                        {{ $result->links() }}
                    </div>
              </div>
            </div>
          </div>
    </div>
@endsection
